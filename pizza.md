URI /-/ Opération /-/ MIME /-/ Requete /-/ Réponse
/pizzas /-/ GET /-/ <-application/json <-application/xml /-/  /-/ Liste des pizzas
/pizzas/{id} /-/ GET /-/ <-application/json <-application/xml /-/ /-/ Une pizza ou 404
/pizzas/{id}/{name} /-/ GET /-/ <-text/plain /-/ /-/ Le nom de la pizza ou 404
/pizzas /-/ POST /-/ <-/->application/json->application/x-www-form-urlencoded /-/ Pizza (I1) /-/ Nouvelle pizza (I2) ; 409 si la pizza existe déjà (même nom)
/pizzas/{id} /-/ DELETE /-/  /-/  /-/