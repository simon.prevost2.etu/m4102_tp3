package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;

import java.util.logging.Logger;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

public class PizzaResourceTest extends JerseyTest {
	private static final Logger LOGGER = Logger.getLogger(IngredientResourceTest.class.getName());
    private PizzaDao dao;
    
    @Override
    protected Application configure() {
    	BDDFactory.setJdbiForTests();

        return new ApiV1();
    }
    
    @Before
    public void setEnvUp() {
    	dao = BDDFactory.buildDao(PizzaDao.class);
        dao.createTable();
    }

    @After
    public void tearEnvDown() throws Exception {
    	dao.dropTable();
    }
    
    @Test
    public void testGetExistingPizza() {

        Pizza pizza = new Pizza();
        pizza.setName("Cannibale");
        dao.insert(pizza);

        Response response = target("/ingredients").path(pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
        assertEquals(pizza, result);
    }
}
