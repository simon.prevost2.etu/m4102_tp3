package fr.ulille.iut.pizzaland.resources;

import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

@Produces("application/json")
@Path("/pizzas")
public class PizzaResource {
	private static final Logger LOGGER = Logger.getLogger(IngredientResource.class.getName());
	
	private PizzaDao pizzas;
	
	@Context
    public UriInfo uriInfo;

    public PizzaResource() {
    	pizzas = BDDFactory.buildDao(PizzaDao.class);
        pizzas.createTable();
    }
    
    @GET
    public List<PizzaDto> getAll() {
        LOGGER.info("IngredientResource:getAll");

        List<PizzaDto> l = pizzas.getAll().stream().map(Pizza::toDto).collect(Collectors.toList());
        LOGGER.info(l.toString());
        return l;

    }
    
    @GET
    @Path("{id}")
    @Produces({ "application/json", "application/xml" })
    public PizzaDto getOnePizza(@PathParam("id") UUID id) {
    	LOGGER.info("getOnePizza(" + id + ")");
    	try {
    		Pizza pizza = pizzas.findById(id);
    		LOGGER.info(pizza.toString());
    		return Pizza.toDto(pizza);
    	} catch( Exception e) {
    		throw new WebApplicationException(Response.Status.NOT_FOUND);
    	}
    }
}
