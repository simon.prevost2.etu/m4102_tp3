package fr.ulille.iut.pizzaland.beans;

import java.util.Arrays;
import java.util.UUID;

import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	private UUID id = UUID.randomUUID();
    private String name;
    private Ingredient[] ingredients;
    
    public Pizza() {    	
    }
    
    public Pizza(String name) {
        this.name = name;
    }
    
    public Pizza(UUID id, String name) {
        this.id = id;
        this.name = name;
    }
    
    public Pizza(UUID id, String name, Ingredient[] ingredients) {
        this.id = id;
        this.name = name;
        this.ingredients = ingredients;
    }

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Ingredient[] getIngredients() {
		return ingredients;
	}

	public void setIngredients(Ingredient[] ingredients) {
		this.ingredients = ingredients;
	}
	
	public static PizzaDto toDto(Pizza i) {
        PizzaDto dto = new PizzaDto();
        dto.setId(i.getId());
        dto.setName(i.getName());

        return dto;
    }
    
    public static Pizza fromDto(PizzaDto pizzaDto) {
    	Pizza pizza = new Pizza();
    	pizza.setId(pizzaDto.getId());
    	pizza.setName(pizzaDto.getName());

        return pizza;
    }
	

	@Override
	public String toString() {
		return "Pizza [id=" + id + ", name=" + name + ", ingredients=" + Arrays.toString(ingredients) + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + Arrays.hashCode(ingredients);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (!Arrays.equals(ingredients, other.ingredients))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
    
}
