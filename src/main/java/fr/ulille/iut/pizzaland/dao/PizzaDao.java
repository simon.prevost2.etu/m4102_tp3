package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {
	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
    void createTable();
	
	@SqlUpdate("DROP TABLE IF EXISTS pizzas")
    void dropTable();

    @SqlUpdate("INSERT INTO pizzas (id, name) VALUES (:id, :name)")
    void insert(@BindBean Pizza pizza);

    @SqlUpdate("DELETE FROM pizzas WHERE id = :id")
    void remove(@Bind("id") UUID id);

    @SqlQuery("SELECT * FROM pizzas WHERE name = :name")
    @RegisterBeanMapper(Pizza.class)
    Pizza findByName(@Bind("name") String name);

    @SqlQuery("SELECT * FROM pizzas")
    @RegisterBeanMapper(Pizza.class)
    List<Pizza> getAll();

    @SqlQuery("SELECT * FROM pizzas WHERE id = :id")
    @RegisterBeanMapper(Pizza.class)
    Pizza findById(@Bind("id") UUID id);
}
